package utilities

import (
	"artist/internal/consts"
	"artist/internal/entities"
	"fmt"
	"strings"

	"github.com/gin-gonic/gin"
)

// Getting error codes from context
func GetErrorCodes(ctx *gin.Context) map[string]interface{} {
	contextError, _ := GetContext[map[string]interface{}](ctx, consts.ContextErrorResponses)
	return contextError
}

// Function to retrieve error functions
func ParseFields(ctx *gin.Context, errorType string, fields string, errorsValue map[string]interface{}, endpoint string) (interface{}, bool, float64) {

	var responseJSON interface{}
	// Access the errors value
	errorsMap, exists := errorsValue[consts.Errors].(map[string]interface{})
	if !exists {
		return nil, false, 0
	}

	// Access the endpoint-specific error map
	allErrorMap, exists := errorsMap[consts.AllError].(map[string]interface{})
	if !exists {
		return nil, false, 0
	}

	// other error msg handling.
	if errorType != consts.ValidationErr {
		errOthers := allErrorMap[errorType].(map[string]interface{})
		errCodeVal := errOthers[consts.ErrorCode]
		errVal := errCodeVal.(float64)
		responseJSON = GenerateResponse(errorType, errCodeVal, nil)
		return responseJSON, false, errVal
	}

	validationErrorMap, exists := allErrorMap[consts.ValidationErr].(map[string]interface{})
	errorCodeVal := validationErrorMap[consts.ErrorCode]
	if !exists {
		return nil, false, errorCodeVal.(float64)
	}

	insideErrors, _ := validationErrorMap[consts.Errors].(map[string]interface{})

	pairs := strings.Split(fields, ",")
	fieldErrors := make(map[string]interface{})

	for _, pair := range pairs {
		parts := strings.Split(pair, ":")

		fieldName := parts[0]
		errorKeys := strings.Split(parts[1], "|")

		fieldErrorMap, fieldExists := insideErrors[endpoint].(map[string]interface{})
		if !fieldExists {
			continue
		}

		fieldErrorMessages := make([]string, 0)
		fieldSpecificErrors, fieldSpecificExists := fieldErrorMap[fieldName].(map[string]interface{})

		if !fieldSpecificExists {
			continue
		}

		for _, key := range errorKeys {
			if errorValue, ok := fieldSpecificErrors[key].(string); ok {
				fieldErrorMessages = append(fieldErrorMessages, errorValue)
			}
		}

		if len(fieldErrorMessages) > 0 {
			fieldErrors[fieldName] = fieldErrorMessages
		}
	}
	responseJSON = GenerateResponse(consts.ValidationErr, errorCodeVal.(float64), fieldErrors)
	return responseJSON, true, errorCodeVal.(float64)
}

// Function to generate error response format
func GenerateResponse(message string, errorCode interface{}, errorsValue map[string]interface{}) interface{} {
	response := entities.ErrorResponse{
		Message:   message,
		ErrorCode: errorCode,
		Errors:    errorsValue,
	}
	return response
}

// Function to generate fields of the form firstname:required|valid
func GenerateFields(fieldsMap map[string][]string) string {
	var fields []string

	for fieldName, errorKeys := range fieldsMap {
		errorKeyString := strings.Join(errorKeys, "|")
		field := fmt.Sprintf("%s:%s", fieldName, errorKeyString)
		fields = append(fields, field)
	}

	return strings.Join(fields, ",")
}

// Function to generate field values key-value pair of the form firstname:required|valid
func FieldValuesToMap(m map[string][]string, key string, values ...string) {
	if _, exists := m[key]; exists {
		m[key] = append(m[key], values...)
	} else {
		m[key] = values
	}
}
