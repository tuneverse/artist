package consts

const (
	DatabaseType     = "postgres"
	AppName          = "artist"
	AcceptedVersions = "v1"
)

const (
	ContextAcceptedVersions       = "Accept-Version"
	ContextSystemAcceptedVersions = "System-Accept-Versions"
	ContextAcceptedVersionIndex   = "Accepted-Version-index"
)

const (
	ContextErrorResponses        = "context-error-response"
	ContextLocallizationLanguage = "lan"
)

// headers
const (
	HeaderLocallizationLanguage = "Accept-Language"
)

// cache name
const CacheErrorData = "CACHE_ERROR_DATA"
const ExpiryTime = 180

// KeyNames
const (
	ValidationErr     = "validation_error"
	ForbiddenErr      = "forbidden"
	UnauthorisedErr   = "unauthorized"
	NotFound          = "not found"
	InternalServerErr = "internal_server_error"
	Errors            = "errors"
	AllError          = "AllError"
	Registration      = "registration"
	ErrorCode         = "errorCode"
	MemberIdErr       = "member_id"
)
