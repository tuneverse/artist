package middlewares

import (
	"artist/config"
	"artist/internal/consts"
	"artist/internal/entities"
	"artist/utilities"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/patrickmn/go-cache"
	log "github.com/sirupsen/logrus"
)

type Middlewares struct {
	Cache           *cache.Cache
	CacheExpiration int `default:"1" split_words:"true"`
	Cfg             *entities.EnvConfig
}

// NewMiddlewares
func NewMiddlewares(cfg *entities.EnvConfig) *Middlewares {
	return &Middlewares{
		Cache: cache.New(5*time.Minute, 10*time.Minute),
		Cfg:   cfg,
	}
}

// Middleware function to check Accept-version from API Header
func (m Middlewares) ApiVersioning() gin.HandlerFunc {
	return func(c *gin.Context) {
		version := c.Param("version")
		if version == "" {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "Missing version parameter"})
			return
		}

		apiVersion := utilities.PrepareVersionName(version)
		apiVersion = strings.ToUpper(apiVersion)

		// set the accepting version in the context
		c.Set(consts.AcceptedVersions, apiVersion)

		// init the system Accepted versions
		// init the env config
		cfg, err := config.LoadConfig(consts.AppName)
		if err != nil {
			panic(err)
		}

		// set the list of system accepting version in the context
		systemAcceptedVersionsList := cfg.AcceptedVersions
		c.Set(consts.ContextSystemAcceptedVersions, systemAcceptedVersionsList)

		// check the version exists in the accepted list
		// find index of version from Accepted versions
		var found bool
		for index, version := range systemAcceptedVersionsList {
			version = strings.ToUpper(version)
			if version == apiVersion {
				found = true
				c.Set(consts.ContextAcceptedVersionIndex, index)
			}

		}
		if !found {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "Given version is not supported by the system"})
			return
		}

		c.Next()
	}
}

// LocalizationLanguage
func (m Middlewares) LocalizationLanguage() gin.HandlerFunc {
	return func(c *gin.Context) {
		// check it is exists in the header
		lan := c.Request.Header.Get(consts.HeaderLocallizationLanguage)
		if lan == "" {
			lan = "en"
		}
		// setting the language
		c.Set(consts.ContextLocallizationLanguage, lan)
		c.Next()
	}
}

// Middleware function to check header value
func (m Middlewares) AuthorisationCheck() gin.HandlerFunc {
	return func(c *gin.Context) {
		memberUuid := utilities.GetHeader(c, "uuid")
		if memberUuid == "" {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": "header value missing"})
			return
		}
		c.Next()
	}
}

// Middlware function to implement localization

func (m Middlewares) ErrorLocalization() gin.HandlerFunc {
	return func(c *gin.Context) {
		//for storing error response data from context
		var errorData = make(map[string]interface{})
		cacheData, isFound := m.Cache.Get(consts.CacheErrorData)
		//check whether data is in cache or not
		if isFound {
			log.Infof("found data in cache")
			c.Set(consts.ContextErrorResponses, cacheData)
		} else {
			localisationUrl := fmt.Sprintf("%s/localization/error",
				m.Cfg.LocalisationServiceURL)
			log.Infof("[ErrorLocalization] Cache not found, calling downstream API")
			language, ok := utilities.GetContext[string](c, consts.ContextLocallizationLanguage)
			if !ok {
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
					"error": "Unable to find the localization language"})
				return
			}

			headers := map[string]interface{}{
				consts.HeaderLocallizationLanguage: language,
			}

			resp, err := utilities.APIRequest(http.MethodGet, localisationUrl, headers, nil)
			if err != nil {
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
					"error": "An unexpected error occured"})
				return
			}

			//checking the status code
			if resp.StatusCode != http.StatusOK {
				log.Errorf("unable to read data from localisation response %v", err)
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
					"error": "unable to read data from localisation response"})
				return
			}
			body, err := io.ReadAll(resp.Body)
			if err != nil {
				log.Errorf("unable to read data from localisation response %v", err)
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
					"error": fmt.Errorf("unable to read data from localisation response 				%v", err).Error()})
				return
			}
			defer resp.Body.Close()

			err = json.Unmarshal(body, &errorData)
			if err != nil {
				log.Errorf("unable to read data from localisation response %v", err)
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
					"error": "unable to read data from localisation response"})
				return
			}

			// set error data in context
			c.Set(consts.ContextErrorResponses, errorData)

			m.Cache.Set(consts.CacheErrorData, errorData,
				time.Duration(m.CacheExpiration)*time.Minute)
			// set error data in cache with an expiration time
			cacheExpiration, err := time.ParseDuration(fmt.Sprintf("%dm", m.CacheExpiration))
			if err != nil {
				log.Errorf("error parsing cache expiration duration: %v", err)
			} else {
				m.Cache.Set(consts.CacheErrorData, errorData, cacheExpiration)
			}
		}
		c.Next()
	}
}
