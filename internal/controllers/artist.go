package controllers

import (
	"artist/internal/usecases"
	"artist/version"
	"net/http"

	"github.com/gin-gonic/gin"
)

type ArtistController struct {
	router   *gin.RouterGroup
	useCases usecases.ArtistUseCaseImply
}

// NewUserController
func NewArtistController(router *gin.RouterGroup, artistUseCase usecases.ArtistUseCaseImply) *ArtistController {
	return &ArtistController{
		router:   router,
		useCases: artistUseCase,
	}
}

// InitRoutes
func (artist *ArtistController) InitRoutes() {
	// user.router.Handle(http.MethodGet, "/health", func(ctx *gin.Context) {
	// 	version.RenderHandler(ctx, user, "HealthHandler")
	// })

	artist.router.GET("/:version/health", func(ctx *gin.Context) {
		version.RenderHandler(ctx, artist, "HealthHandler")
	})
}

// HealthHandler
func (artist *ArtistController) HealthHandler(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "server run with base version",
	})
}
