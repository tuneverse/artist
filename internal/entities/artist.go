package entities

type Artist struct{}

type ErrorResponse struct {
	Message   string                 `json:"message"`
	ErrorCode interface{}            `json:"errorCode"`
	Errors    map[string]interface{} `json:"errors"`
}
