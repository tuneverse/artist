package repo

import (
	"database/sql"
)

type ArtistRepo struct {
	db *sql.DB
}

type ArtistRepoImply interface {
}

// NewUserRepo
func NewArtistRepo(db *sql.DB) ArtistRepoImply {
	return &ArtistRepo{db: db}
}
