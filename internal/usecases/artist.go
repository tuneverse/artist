package usecases

import (
	"artist/internal/repo"
)

type ArtistUseCases struct {
	repo repo.ArtistRepoImply
}

type ArtistUseCaseImply interface {
}

// NewUserUseCases
func NewArtistUseCases(artistRepo repo.ArtistRepoImply) ArtistUseCaseImply {
	return &ArtistUseCases{
		repo: artistRepo,
	}
}
